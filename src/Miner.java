import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

/**
 * Class for a miner.
 */
public class Miner extends Thread {
	private Integer hashCount;
	private Set<Integer> solved;
	private CommunicationChannel channel;
	
	/**
	 * Creates a {@code Miner} object.
	 *
	 * @param hashCount
	 *            number of times that a miner repeats the hash operation when
	 *            solving a puzzle.
	 * @param solved
	 *            set containing the IDs of the solved rooms
	 * @param channel
	 *            communication channel between the miners and the wizards
	 */
	public Miner(Integer hashCount, Set<Integer> solved, CommunicationChannel channel) {
		this.hashCount = hashCount;
		this.solved = solved;
		this.channel = channel;
	}
	
//		Functiile de encrypt luate din Main.java.
//		Aceste functii vor fi folosite de Mineri pentru a rezolva puzzle-urile
	private static String encryptMultipleTimes(String input, Integer count) {
        String hashed = input;
        for (int i = 0; i < count; ++i) {
            hashed = encryptThisString(hashed);
        }

        return hashed;
    }

    private static String encryptThisString(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] messageDigest = md.digest(input.getBytes(StandardCharsets.UTF_8));

            // convert to string
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
            String hex = Integer.toHexString(0xff & messageDigest[i]);
            if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

	@Override
	public void run() {
//		Minerul va fi activ pana cand va primi mesajul EXIT
		while (true) {			
//			Obtinem mesajul ce descrie camera curenta
			Message message = this.channel.getMessageWizardChannel();
//			Despachetam acest mesaj
			int room = message.getCurrentRoom();
			String data = message.getData();
			
			if (data.equals("END")) {
//				Nu avem o alta camera in care sa mergem, cautam alt mesaj
				continue ;
			}
			else if (data.equals("EXIT")) {
//				Puzzle-ul a fost terminat, minerii pot sa iasa din labirint
				return ;
			}
			
			if (solved.contains(room)) {
//				Camera a fost deja rezolvata, cautam alt mesaj
				continue;
			} else {
//				  Camera va fi rezolvata acum, alti mineri nu trebuie
//				sa intre aici intre timp, pot incerca alta camera
				this.solved.add(message.getCurrentRoom());
			}

//			Minam mesajul folosind functia din Main
			String newData = this.encryptMultipleTimes(data, this.hashCount);

//				Construim mesajul pe care Minerul il va trimite Vrajitorilor
//			inlocuind vechiul mesaj cu cel de raspuns
			message.setData(newData);
			
//			Trimitem mesajul Vrajitorilor
			this.channel.putMessageMinerChannel(message);
		}
	}
}
