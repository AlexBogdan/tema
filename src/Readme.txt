Mazilu Ana-Maria
334CA
Tema 2 APD

	Pentru a implementa aceasta tema am folosit ideea problemei
"Scriitori-Cititori".
	Initial am implementat clasa CommunicationChannel, unde am
completat functiile din schelet pentru a asigura comunicarea intre
Vrajitori si Mineri.
	Pentru canalul Mineri -> Vrajitori am folosit structura de
date "LinkedBlockingDeque" pentru a asigura o citire si o scriere
sincronizata.
	Pentru canalul Vrajitori -> Mineri am folosit LinkedList
simplu, intrucat sincronizarea a fost implemtata de mine. Pentru a
sincroniza m-am folosit de un ReentrantLock si de un Semaphore. In
momentul in care un Vrajitor dorea sa transmita un mesaj catre Mineri,
acesta incerca sa obtina lock-ul (intrucat doar un vrajitor pe rand
poate sa transmita un mesaj). In functie de tipul mesajului, puteam
avea nevoie de 1 sau de 2 mesaje consecutive pentru a forma mesajul
transmis. In cazul in care trebuie sa transmitem mesaje de forma
"parentRoom | currentRoom" asteptam ca acelasi vrajitor sa trimita
aceste 2 mesaje, iar mai apoi parentRoom este setat ca parentId in
currentRoom. Atunci cand un mesaj este adaugat in coada, vom inccrementa
un semafor care sa permita Minerilor sa citeasca noul mesaj introdus.
	In momentul in care este transmis mesajul EXIT, restul mesajelor
din canal sunt sterse, deoarece puzzle-ul a fost deja rezolvat.
	In clasa Miner am copiat functia de hashing din clasa Main.
Functia este folosita de catre Mineri pentru a rezolva mesajele.
Cand un miner termina de aplicat functia hash, noul mesaj va fi
impachetat in mesaj si retransmis Vrajitorilor pentru a genera alte
task-uri.
