import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class that implements the channel used by wizards and miners to communicate.
 */
public class CommunicationChannel {
//	 Canalul de comunicare Miner -> Vrajitor
	private LinkedBlockingDeque<Message> minersChannel;
//	 Canalul de comunicare Vrajitor -> Miner
	private LinkedList<Message> wizardsChannel;
//	Lock-ul pe care un vrajitor il va tine in momentul in care trimite un mesaj
	private ReentrantLock lock;
//	Semaforul ce va spune minerilor cand un Vrajitor a adaugat un mesaj in channel
	private Semaphore minersSemaphore;
//	Mesajul anterior care se va uni cu camera adiacenta
	private Message msg;
//	Ne va spune daca mesajul EXIT a aparut in channel
	static private boolean commEnded = false;

	/**
	 * Creates a {@code CommunicationChannel} object.
	 */
	public CommunicationChannel() {
		this.minersChannel = new LinkedBlockingDeque<Message>();
		this.wizardsChannel = new LinkedList<Message>();
		this.lock = new ReentrantLock();
		this.minersSemaphore = new Semaphore(0);
	}

	/**
	 * Puts a message on the miner channel (i.e., where miners write to and wizards
	 * read from).
	 *
	 * @param message
	 *            message to be put on the channel
	 */
	public void putMessageMinerChannel(Message message) {
		try {
			this.minersChannel.put(message);
		}
		catch(Exception e) {}
	}

	/**
	 * Gets a message from the miner channel (i.e., where miners write to and
	 * wizards read from).
	 *
	 * @return message from the miner channel
	 */
	public Message getMessageMinerChannel() {
		try {
			Message message = this.minersChannel.take();
			
			return message;
		}
		catch(Exception e) {}
		
		return null;
	}

	/**
	 * Puts a message on the wizard channel (i.e., where wizards write to and miners
	 * read from).
	 *
	 * @param message
	 *            message to be put on the channel
	 */
	public void putMessageWizardChannel(Message message) {
//		Vrajitorul vrea sa transmita un mesaj, blocheaza alti Vrajitori
		lock.lock();
		try {
			if (message.getData().equals("EXIT")) {
//					Daca am primit mesajul "EXIT", puzzle-ul a fost finalizat,
//				astfel ca vom goli canalul pentru a nu lasa Minerii sa lucreze inutil
				if (commEnded == false) {
					minersSemaphore.drainPermits();
					this.wizardsChannel.clear();
					commEnded = true;
				}
				this.wizardsChannel.addLast(message);
				minersSemaphore.release();
				lock.unlock();
			}
			else if (message.getData().equals("END")) {
				this.wizardsChannel.addLast(message);
				minersSemaphore.release();
				lock.unlock();
			}
			else if (msg != null) {
//				Am primit si cel de-al 2-lea mesaj, putem trimite un mesaj minerilor
				message.setParentRoom(msg.getCurrentRoom());
				this.wizardsChannel.addLast(message);
				minersSemaphore.release();
				msg = null;
				lock.unlock();
			} else {
//					Am primit doar primul mesaj, il asteptam si pe al 2-lea
//				pentru a forma mesajul real.
				msg = message;
			}
		}
		catch(Exception e) {}
	}

	/**
	 * Gets a message from the wizard channel (i.e., where wizards write to and
	 * miners read from).
	 *
	 * @return message from the miner channel
	 */
	public Message getMessageWizardChannel() {
		try {
//			Minerii vor citi mesaje in momentul in care semaforul le va permite
			minersSemaphore.acquire();
			Message message = this.wizardsChannel.pollFirst();
			
			return message;
		}
		catch(Exception e) {}
		
		return null;
	}
}
